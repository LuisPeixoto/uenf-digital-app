exports.items = [
    {
        title: "Manual do calouro",
        nameIcon: "book",
        typeIcon: 'FontAwesome',
        screen: 'WebView',
        url: "http://plataforma.uenf.br/manual-do-calouro/",
    },

    {
        title: "Associações estudantis",
        nameIcon: "users",
        typeIcon: 'FontAwesome',
        screen: 'WebView',
        url: "http://plataforma.uenf.br/associacoes-estudantis/",
    },

    {
        title: "Projetos de extensão",
        nameIcon: "lab-flask",
        typeIcon: 'Entypo',
        screen: 'WebView',
        url: "http://plataforma.uenf.br/projetos-de-extensao/",
    },

    {
        title: "Iniciação Científica",
        nameIcon: "atom",
        typeIcon: 'FontAwesome5',
        screen: 'WebView',
        url: "https://uenf.br/projetos/pibic/",
    },

    {
        title: "Hospital Veterinário",
        nameIcon: "hospital",
        typeIcon: 'FontAwesome5',
        screen: 'WebView',
        url: "https://hospital-veterinario-uenf.negocio.site/#details",
    },

    {
        title: "Calendários",
        nameIcon: "calendar-alt",
        typeIcon: 'FontAwesome5',
        screen: 'WebView',
        url: "https://uenf.br/portal/ensino/calendarios/",
    },

    {
        title: "Pós-Graduação",
        nameIcon: "graduation-cap",
        typeIcon: 'FontAwesome5',
        screen: 'WebView',
        url: "https://uenf.br/posgraduacao/",
    },

    {
        title: "Sobre a UENF",
        nameIcon: "institution",
        typeIcon: 'FontAwesome',
        screen: 'WebView',
        url: "https://uenf.br/portal/institucional/sobre-a-uenf/",
    },
]