exports.items = [
    {
        title: "Empréstimos e reservas",
        nameIcon: "book",
        category: "Consultar",
        typeIcon: 'AntDesign',
        url: "http://www.bibliotecas.uenf.br/informa/cgi-bin/biblio.dll/emprest?g=geral&bd=&p=GERAL",
    },

    {
        title: "Publicações",
        category: "Consultar",
        nameIcon: "document-text-outline",
        typeIcon: 'Ionicons',
        url: "http://www.bibliotecas.uenf.br/informa/cgi-bin/biblio.dll/book?g=geral&bd=&p=GERAL&unide=&t=",
    },

    {
        title: "Periódicos",
        nameIcon: "md-documents-outline",
        category: "Consultar",
        typeIcon: 'Ionicons',
        url: "http://www.bibliotecas.uenf.br/informa/cgi-bin/biblio.dll/colec?g=geral&bd=&p=GERAL",
    },

    {
        title: "Inclusão",
        category: "Sugestão",
        nameIcon: "book-plus-multiple-outline",
        typeIcon: 'MaterialCommunityIcons',
        url: "http://www.bibliotecas.uenf.br/informa/cgi-bin/biblio.dll/sugest?g=geral&bd=&p=MULTIPLA",
    },
]