exports.items = [
    {
        name: "macrovector / Freepik",
        url: "https://br.freepik.com/vetores-gratis/biblioteca-do-campus-da-universidade-estante-composicao-plana-com-estudantes-lendo-artigos-de-livros-com-estantes_7201294.htm#page=1&query=university%20campus%20library%20bookcases%20flat%20composition%20with%20students%20reading%20books%20articles%20with%20bookshelves&position=0",
        image: require("../../assets/img/assignments/library.png")
    },

    {
        name: "macrovector / Freepik",
        url: "https://br.freepik.com/vetores-gratis/apresentacao-conference-hall-composition_4280549.htm#page=1&query=presentation%20conference%20hall%20composition&position=7",
        image: require("../../assets/img/assignments/events.png")
    },
  
    {
        name: "pch.vector / Freepik",
        url: "https://br.freepik.com/vetores-gratis/menina-escrevendo-em-um-diario-ou-diario_9649836.htm#page=1&query=girl%20writing%20in%20journal%20or%20diary&position=4",
        image: require("../../assets/img/assignments/edicts.png")
    },

    {
        name: "pikisuperstar / Freepik",
        url: "https://br.freepik.com/vetores-gratis/arquivo-que-busca-o-conceito-ilustrado-da-pagina-de-destino_5538692.htm#page=1&query=file%20searching%20illustrated%20concept%20for%20landing%20page&position=0",
        image: require("../../assets/img/assignments/search.png")
    },
    
    {
        name: "vectorjuice / Freepik",
        url: "https://br.freepik.com/vetores-gratis/profissionais-de-ti-estao-criando-web-site-na-ilustracao-da-tela-do-laptop_10780362.htm#page=1&query=it%20professionals%20are%20creating%20web%20site%20on%20the%20laptop%20screen%20illustration&position=0",
        image: require("../../assets/img/assignments/about.png")
    },
]