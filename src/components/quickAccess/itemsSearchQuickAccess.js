exports.itemsQuickAccess = [
    {
        name: 'Bolsas',
        nameIcon: 'book',
        typeIcon: 'AntDesign',

    },

    {
        name: 'Notícias',
        nameIcon: 'newspaper-outline',
        typeIcon: 'Ionicons',

    },

    {
        name: 'Informes',
        nameIcon: 'newspaper',
        typeIcon: 'FontAwesome5',

    },

    {
        name: 'Eventos',
        nameIcon: 'calendar-star',
        typeIcon: 'MaterialCommunityIcons',

    },

    {
        name: 'Associações estudantis',
        nameIcon: 'account-group-outline',
        typeIcon: 'MaterialCommunityIcons',

    },

    {
        name: 'Editais',
        nameIcon: 'documents-outline',
        typeIcon: 'Ionicons',

    }
]