exports.items = [
  {
    title: "Bibliotecas",
    nameIcon: "book",
    IconType: "FontAwesome",
    screen: 'Library'
  },

  {
    title: "Cardápio",
    link: "https://www.instagram.com/stories/uenf_oficial/",
    nameIcon: "restaurant",
    IconType: "Ionicons",
    screen: 'WebView'
  },

  {
    title: "Acadêmico",
    link: 'https://academico.uenf.br/',
    nameIcon: "graduation-cap",
    IconType: "FontAwesome",
    screen: 'WebView'
  },

  {
    title: "Eventos",
    nameIcon: "calendar-star",
    IconType: "MaterialCommunityIcons",
    screen: 'PageScreen'
  },

  {
    title: "Documentos",
    link: "http://plataforma.uenf.br/documentos-academicos/",
    nameIcon: "document-text-outline",
    IconType: "Ionicons",
    screen: 'WebView'
  },

  {
    title: "Editais",
    nameIcon: "documents-outline",
    IconType: "Ionicons",
    screen: 'PageScreen'
  }

]