exports.items = [
    {
        title: "Sobre o aplicativo",
        nameIcon: "screen-smartphone",
        typeIcon: 'SimpleLineIcons',
        screen: "AboutApp",
    },

    {
        title: "Sobre o projeto",
        nameIcon: "people-outline",
        typeIcon: 'Ionicons',
        screen: 'WebView',
        url: "http://plataforma.uenf.br/quem-somos/",
    },
]