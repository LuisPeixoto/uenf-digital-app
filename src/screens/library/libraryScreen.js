import React, { useEffect } from 'react';
import OptionsLibrary from '../../components/library/optionsLibrary'

function Library({ navigation }) {
    {
        return <OptionsLibrary navigation={navigation}/>
    }
}

export default Library;

