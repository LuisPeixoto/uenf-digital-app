import React, { useEffect } from 'react';
import AppAbout from '../../components/about/appAbout'

function AboutAppScreen({ navigation }) {
    {
        return <AppAbout navigation={navigation}/>
    }
}

export default AboutAppScreen;

