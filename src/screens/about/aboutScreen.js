import React, { useEffect } from 'react';
import OptionsAbout from '../../components/about/optionsAbout'

function aboutScreen({ navigation }) {
    {
        return <OptionsAbout navigation={navigation}/>
    }
}

export default aboutScreen;

