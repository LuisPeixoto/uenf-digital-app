import React, { useEffect } from 'react';
import Search from '../../components/search'

function searchScreen({navigation}) {
   {
    return (
        <Search navigation={navigation} />
    )
  }
}

export default searchScreen;
