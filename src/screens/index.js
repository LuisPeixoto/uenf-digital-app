import HomeScreen from './home/homeScreen'
import WebViewScreen from './postsWebview/postsWebviewScreen'
import LibraryWebviewScreen from './library/LibraryWebview'
import NotificationScreen from './notification/notificationScreen'
import LibraryScreen from './library/libraryScreen'
import SearchScreen from './search/searchScreen'
import PageScreen from './page/pageScreen'


export default  {
    HomeScreen,
    WebViewScreen,
    LibraryWebviewScreen,
    NotificationScreen,
    LibraryScreen,
    SearchScreen,
    PageScreen,
}